/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path');

exports.createPages = async ({ actions, graphql }) => {
    const { createPage } = actions;

    const questions = await graphql(`
        {
            allNodeQuestion {
                nodes {
                    id
                    title
                    field_question
                    field_option_values
                    field_option
                    relationships {
                        field_next_question {
                            id
                        }
                        field_option_referenced_question {
                            id
                        }
                    }
                    field_isfinal
                }
            }
        }   
    `);

    questions.data.allNodeQuestion.nodes.map ( questionData =>
        createPage({
            path: questionData.id,
            component: path.resolve(`src/templates/questionary.js`),
            context: {
                QuestionId: questionData.id,
            },
        })
    );
}

exports.onCreatePage = async ({ page, actions }) => {
    const { createPage } = actions
    // page.matchPath is a special key that's used for matching pages
    // only on the client.
    if (page.path.match(/^\/app/)) {
      page.matchPath = "/app/*"
      // Update the page.
      createPage(page)
    }
  }

  exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    if (stage === 'build-html') {
      /*
       * During the build step, `auth0-js` will break because it relies on
       * browser-specific APIs. Fortunately, we don’t need it during the build.
       * Using Webpack’s null loader, we’re able to effectively ignore `auth0-js`
       * during the build. (See `src/utils/auth.js` to see how we prevent this
       * from breaking the app.)
       */
      actions.setWebpackConfig({
        module: {
          rules: [
            {
              test: /auth0-js/,
              use: loaders.null()
            }
          ]
        }
      });
    }
  }