import React from "react"
import Layout from "../components/layout"
import PrivateRoute from "../components/privateRoute"
import WelcomePage from "../pages/welcome"
import { Router } from "@reach/router"

const App = () => (
  <Layout>
    <Router>
        <PrivateRoute path="/welcome" component={WelcomePage} />
    </Router>
  </Layout>
)

export default App