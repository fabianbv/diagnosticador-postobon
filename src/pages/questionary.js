import * as React from "react"
import { graphql, navigate } from "gatsby"
import { Helmet } from 'react-helmet'
import PropTypes from 'prop-types';
import { isLoggedIn } from "../services/auth"
import Layout from "../components/layout"

import titleImg from '../images/illustrations/deco3.svg'
// import { handleLogin, isLoggedIn } from "../services/auth"

const node_url = `${process.env.GATSBY_DRUPAL_ROOT}node?_format=json`;
const node_type_url = `${process.env.GATSBY_DRUPAL_ROOT}rest/type/node/results`;

class Questionary extends React.Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        next: PropTypes.string,
        flag: PropTypes.bool,
        porcent: PropTypes.number,
        cat: PropTypes.string,
        score: PropTypes.string,
        
        tools: PropTypes.number,
        strategy: PropTypes.number,
        operation: PropTypes.number,
        experience: PropTypes.number,
    }

    flag = true
    porcent = Math.floor((100 / this.props.data.allNodeQuestion.nodes.length))
    score = 0
    tools = 0
    strategy = 0
    operation = 0
    experience = 0
    average = 0
    
    state = {
        active: false,
        ids: [],
        activeId: null,
        url: 1,
        back: null,
        setBack: null,
        category: null,
        error: null,
        resultsData: {
            cat: {
                tools: '',
                strategy: '',
                operation: '',
                experience: ''
            }
        }
    }
    
    handleClick = (e, id, url, backU, points) => {
        this.url = url
        this.setBack = backU
        this.setState({ activeId: id })
        this.flag = isNaN(this.url) ? false : this.flag
        this.score = points
    };

    handleClick2(id) {
        this.setState(oldState => ({
            ids: oldState.ids.includes(id)
              ? oldState.ids.filter(x => x !== id)
              : [...oldState.ids, id]
        }));
        // console.log(e.target);
      }

    handleBack = ( event, cat ) => {
        if(event.target.dataset.url === undefined){
            navigate('/welcome');
        }else{
            event.preventDefault()
            // navigate(-1);
            this.next = event.target.dataset.url
            this.forceUpdate()
            this.setState({ activeId: null, ids: [] })
            this.porcent = Math.floor((100 / this.props.data.allNodeQuestion.nodes.length) * this.next)
            if(cat.name === 'Strategy'){
                this.strategy -= this.score
            }else if(cat.name ==='Operation'){
                this.operation -= this.score
            }else if(cat.name === 'Digital user experience'){
                this.experience -= this.score
            }else if(cat.name === 'Complementary tools'){
                this.tools -= this.score
         
            }
        }
    }

    handleRender = ( event, cat ) => {
        if(this.flag === true){
            event.preventDefault()
            this.next = event.target.dataset.url
            this.forceUpdate()
            this.setState({ activeId: null, ids: [] })
            this.back = this.setBack
            this.porcent = Math.floor((100 / this.props.data.allNodeQuestion.nodes.length) * this.next)
            if(cat.name === 'Strategy'){
                this.strategy += this.score
            }else if(cat.name === 'Operation'){
                this.operation += this.score
            }else if(cat.name === 'Digital user experience'){
                this.experience += this.score
            }else if(cat.name === 'Complementary tools'){
                this.tools += this.score
            }
            this.score = null
            if(this.state.activeId == null){
                this.error = "Por favor selecciona una opción"
            }else{
                this.error = ""
            }
            console.log(this.strategy + '/' + this.operation + '/' + this.experience + '/' + this.tools) 
        }else{
            if(cat.name === 'Strategy'){
                this.strategy += this.score
            }else if(cat.name === 'Operation'){
                this.operation += this.score
            }else if(cat.name === 'Digital user experience'){
                this.experience += this.score
            }else if(cat.name === 'Complementary tools'){
                this.tools += this.score
            }
            this.state.resultsData.cat.tools = this.tools
            this.state.resultsData.cat.strategy = this.strategy
            this.state.resultsData.cat.experience = this.experience
            this.state.resultsData.cat.operation = this.operation
            

            async function fetchUrl(tools, experience, operation, strategy) {
                const average = (tools + experience + operation + strategy)/4;
                const token = localStorage.getItem('access-token') !== null ? JSON.parse(localStorage.getItem('access-token')) : null;
                const response = await fetch(node_url, {
                    method: "POST",
                    headers: {
                        "Accept": "*/*",
                        "Content-Type": "application/hal+json",
                        "Authorization": `${token.token_type} ${token.access_token}`,
                    },
                    body: JSON.stringify({
                        _links: { type: { href: node_type_url} },
                        type: { target_id: "results"},
                        title: { value: "ExampleR2esults"},
                        field_complementary_tools: { value: tools},
                        field_digital_user_experience: { value: experience},
                        field_operation: { value: operation},
                        field_strategy: { value: strategy},
                        field_average: { value: average},
                      }),
                });

                if (response.ok) {
                    const json = await response.json();
                    if (json.error) {
                      throw new Error(json.error.message);
                    }
                    console.log(json);
                    navigate('/results');
                    return json;
                    
                }
            }

            fetchUrl(this.tools, this.experience, this.operation, this.strategy);
            
        }
    }
    

    render() {
        isLoggedIn().then((res) => {
            if(res == false){
                if(typeof window !== 'undefined'){
                    localStorage.removeItem('access-token');
                    return navigate('/');  
                }
            }
          });  
        const allQuest = this.props.data.allNodeQuestion
        const nextUrl = this.url || 1
        const checkTitle = this.next || 1
        const backUrl = this.back
        const porcent =  this.porcent
        let catt = this.props.cat
        // porcent = porcent * nextUrl
        // console.log(nextUrl)
        return(
            <>
                <Helmet>
                    <body class='question-template' />
                </Helmet>
                <Layout>
                    
                    <div className='question'>
                        <div className='question-container'>
                            
                            <div className='question-progress'><span style={{ width: porcent + `%` }}></span></div>
                            {allQuest.nodes.map( (quest , index) => {
                                if(quest.title == checkTitle){
                                    let ref1 = 'results/';
                                    let back = quest.title
                                    if(quest.relationships.field_option_referenced_question.length !== 0){
                                        ref1 = quest.relationships.field_option_referenced_question
                                    }else{
                                        if(quest.field_isfinal === false){
                                            ref1 = quest.relationships.field_next_question
                                        }else{
                                            ref1 = 'results/'
                                        }
                                    }
                                    return (
                                        <>
                                        <div className= 'question-index'>
                                            <span>{quest.title}</span>
                                            <img src={titleImg} alt='question' />
                                        </div>
                                        <h1 className='question-title'>{quest.field_question}</h1>
                                        <p className='question-intro'>{quest.field_option.length > 3 ? 'Selecciona todas las opciones que apliquen.' : 'Selecciona la opción que aplica'}</p>
                                        <span className='errorMsg'>{this.error}</span>
                                        <div className='options-container'>
                                            {quest.field_option.map( (preg , index) => {
                                                catt = quest.relationships.field_category;
                                                // if(quest.title === 2 || quest === 4){
                                                //     <div>multi</div>
                                                // }else{
                                                //     <div>default</div>
                                                // }
                                                return (
                                                    quest.field_option.length > 3 ? 
                                                    <div key={ index } className='options-container-item'>
                                                        <div key={ index } className={`options-item ` + (this.state.ids.includes(index) ? "active" : "")} aria-hidden="true" onClick={(e)=>{this.handleClick(this, index, ref1[index] ? ref1[index].title : ref1.title ? ref1.title : ref1, back, quest.field_option_values[index]); this.handleClick2(index)}}>{preg}</div>
                                                    </div> : 
                                                    <div key={ index } className='options-container-item'>
                                                        <div key={ index } className={`options-item ` + (this.state.activeId === index ? `active` : '')} aria-hidden="true" onClick={(e)=>{this.handleClick(this, index, ref1[index] ? ref1[index].title : ref1.title ? ref1.title : ref1, back, quest.field_option_values[index]); this.handleClick2(index)}}>{preg}</div>
                                                    </div>
                                                );
                                            })}
                                        </div>
                                        </>
                                    );
                                }
                            })}
                            <div className='question-btns'>
                                <a className='main-btn main-btn--blue' role = "button" tabIndex={0} data-url={nextUrl} onClick={(e) =>{this.handleRender(e, catt)}}>Siguiente</a>
                                <a className='main-btn  main-btn--border' role = "button" tabIndex={0} data-url={backUrl} onClick={(e) =>{this.handleBack(e, catt)}}>Volver</a>
                                {/* <a className='main-btn main-btn--green' data-url={nextUrl} onClick={this.handleRender}>Siguiente</a> */}
                            </div>
                    </div>
                </div>
                </Layout>
            </>
        )
    }
}

export const query = graphql`
    query Questionary {
        allNodeQuestion {
            nodes {
                field_option
                field_option_values
                field_question
                id
                title
                relationships {
                    field_next_question {
                        id
                        title
                    }
                    field_option_referenced_question {
                        id
                        title
                    }
                    field_category {
                        name
                    }
                }
                field_isfinal
            }
        }
    }
`;

export default Questionary;