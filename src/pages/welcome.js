import * as React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

import bannerImg from '../images/illustrations/deco2.svg'

const WelcomePage = ({data}) => (
  <Layout>
    <Seo title="Welcome page" />
    <div className='hero-banner hero-banner-welcome'>
      <div className='hero-banner-container hero-banner-container--oneColumn'>
        <div className='hero-banner-content'>
          <p className='big-text'>{data.nodePage.relationships.field_custom_blocks[0].field_secction_title}</p>
          <h1 className='main-title'>{data.nodePage.relationships.field_custom_blocks[0].field_secction_subtitle}</h1>
          <img className='hide-desktop' src={bannerImg} alt='Evolucion digital e-commerce'/>
          {/* <p>Si llegaste hasta aquí es porque te interesa explotar las herramientas del internet para potenciar tus ventas y ser un referente en tu sector.</p> */}
          <div dangerouslySetInnerHTML={{__html: data.nodePage.relationships.field_custom_blocks[0].body.processed}}/>
          <Link className='main-btn main-btn--blue' to="/questionary">{data.nodePage.relationships.field_custom_blocks[0].field_secction_button}</Link>
        </div>
        <img className='hide-mobile' src={bannerImg} alt='Evolucion digital e-commerce'/>
      </div>
    </div>
  </Layout>
)

export const query = graphql`
query WelcomePage {
  nodePage(title: {eq: "Welcome Page"}) {
    title
    relationships {
      field_custom_blocks {
        ... on node__secction {
          field_secction_title
          field_secction_subtitle
          body {
            processed
          }
          field_secction_button
        }
      }
    }
  }
}


`;

export default WelcomePage
