import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { handleRecoveryPassword } from "../services/auth"
import Eye from '../images/icons/eye-icon.png'
import EyeOff from '../images/icons/eye-off-icon.png'

class ResetPassword extends React.Component {  
  state = {
    input: '',
    password: '',
    new_password: '',
    error: '',
    passShown: false,
    passShownConf: false,
    success: true,
  }
  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }
  validate = () => {
    let passwordConfirmError = "";
    let passwordError = "";
    let match = "";
    if (!this.state.password) {
      passwordError = "El campo no puede ser vacio";
    }
    if (!this.state.new_password) {
      passwordConfirmError = "El campo no puede ser vacio";
    }
    if (this.state.password !== this.state.new_password && passwordConfirmError === '') {
      passwordConfirmError = "Las contraseñas son diferentes";
    }
    if (passwordError === '' && this.state.password.length < 8) {
        passwordError = "El campo debe tener minímo 8 caracteres";
    }
    if (passwordConfirmError === '' && this.state.new_password.length < 8) {
      passwordConfirmError = "El campo debe tener minímo 8 caracteres";
    }
    match = /^[A-Z]/;
    if (passwordError === "" && !match.exec(this.state.password)) {
      passwordError = "El campo debe tener al menos una mayúscula";
    }
    match = /[a-z]/;
    if (passwordError === "" && !match.exec(this.state.password)) {
      passwordError = "El campo debe tener al menos una minuscula";
    }
    match = /[^a-zA-Z]/;
    if (passwordError === "" && !match.exec(this.state.password)) {
      passwordError = "El campo debe tener al menos un caracter Especial";
    }
    if (passwordConfirmError || passwordError) {
      console.log(passwordConfirmError,passwordError)
      this.setState({passwordConfirmError, passwordError});
      return false;
    };
    return true;
  }
  checkUrlFunction = () => {
    if (typeof window !== 'undefined') {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);
      const recovery= urlParams.get('recovery')

      if(recovery) window.localStorage.setItem('recovery', recovery)
      return recovery;
    };
  };

  handlePass = event => {
    if(this.state.passShown == false || this.state.passShown == undefined){
      this.setState({passShown: true})
    }else{
      this.setState({passShown: false})
    }
  }
  
  handlePass2 = event => {
    if(this.state.passShownConf == false || this.state.passShownConf == undefined){
      this.setState({passShownConf: true})
    }else{
      this.setState({passShownConf: false})
    }
  }
  
  handleSubmit = event => {
    event.preventDefault();
    const isValid = this.validate();
    //const isValid =true;
    const recovery = this.checkUrlFunction();
    if (isValid) {
      this.setState({ processing: true });
      const { password } = this.state;
      // const {recovery} = this.props;
      // console.log(this.props)
      
      handleRecoveryPassword(recovery,password).then((res) => {        
        // debug;
        let error = '';
        if(res != undefined && res) {
          // console.log(res.message);
          if(res.message){
            error = res.message;
          }else{
            error = 'Se ha realizado el cambio de contraseña intente ingresar  <a href="/">aqui</a>';
            this.setState({success: true})
          }
          this.setState({
            processing: true
          })
          this.setState({error});
          // this.error = "Se a realizado el cambio de contraseña intenta ingresar de nuevo"
        }else {
          this.setState({
            processing: false
          })
          error = "Error al recuperar contraseña por favor intenta mas tarde";
          this.setState({error});
          // this.error = "Error al recuperar contraseña por favor intenta mas tarde"
        }
      })      
    }
  }
  
  render() {
    // const queryString = window.location.search;
    // const params = new URLSearchParams(queryString);
    // console.log(params.get("recovery"));
    
    return(
      <Layout>
        <Seo title="Forgot Password" />
        <div className='hero-banner hero-banner-welcome forgotPassword'>
          <div className='hero-banner-container hero-banner-container--oneColumn'>
            <div className='hero-banner-content resetPass'>
              <p className='big-text'>Ingresa tu nueva contraseña</p>
              <p><strong>La contraseña de tener: </strong>
                <div>
                  <span>• Un caracter Especial</span>
                  <span>• Una mayúscula</span>
                  <span>• Una minúscula</span>
                  <span>• Tener minímo 8 caracteres</span>
                </div>
              </p>
              {/* <span className='errorMsg'>{ this.error }</span> */}
              <span className={this.state.success? 'errorMsg msgSuccess' : 'errorMsg'} dangerouslySetInnerHTML={{__html: this.state.error}}></span>
              <form method="post"
                  onSubmit={event => {
                    this.handleSubmit(event)
                  }}>
                <div className="errorMsg">{this.state.error ? '': this.state.passwordError}</div>
                <div className="inputGroup">
                  <input
                    name="password"
                    type={this.state.passShown ? 'text' : 'password' }
                    placeholder='Nueva contraseña'
                    onChange={this.handleInputChange}
                  />
                  <div className="showPass showPass--dark" onClick={this.handlePass}><img src={this.state.passShown ? EyeOff : Eye } alt='pass' /></div>
                </div>
                <div className="errorMsg">{this.state.error ? '': this.state.passwordConfirmError}</div>
                <div className="inputGroup">
                  <input
                    name="new_password"
                    type={this.state.passShownConf ? 'text' : 'password' }
                    placeholder='Confirmar contraseña'
                    onChange={this.handleInputChange}
                  />
                  <div className="showPass showPass--dark" onClick={this.handlePass2}><img src={this.state.passShownConf ? EyeOff : Eye } alt='pass' /></div>
                </div>
                <button className='main-btn' type="submit">Restaurar contraseña</button>
              </form>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default ResetPassword
