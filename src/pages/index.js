import * as React from "react"
// import { Component } from 'react'
import { graphql, Link, navigate } from "gatsby"
import PropTypes from 'prop-types';
// import { Router } from "@reach/router"
import { handleLogin, isLoggedIn } from "../services/auth"

import Layout from "../components/layout"
import Seo from "../components/seo"
import Slider from "react-slick"
import ReactModal from 'react-modal'
import Eye from '../images/icons/eye-icon.png'
import EyeOff from '../images/icons/eye-off-icon.png'


ReactModal.setAppElement('#___gatsby')

const settings = {
  dots: false,
  arrows: true,
  infinite: true,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
        arrows: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
      }
    }
  ]
};

class IndexPage extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }
  constructor(props) {
    super(props)
    this.state = {
      isModalOpen: false,
      processing: false,
    }
    this.error = ''
  }

  handleModalOpen = event => {
    // console.log(isLoggedIn())
    isLoggedIn().then((res) => {
      // console.log(res);
      if(res == false){
        this.setState({ isModalOpen: true })  
      }else{
        navigate("/welcome");
      }
    });
    // console.log('handleModalOpen: ', event);
    //this.setState({ isModalOpen: true })
  }

  handleModalClose = event => {
    // console.log('handleModalOpen: ', event);
    this.setState({ isModalOpen: false })
    this.error = ''
  }

  state = {
    username: '',
    password: '',
    checkbox: false,
    error: null,
    passShown: false,
    // processing: false,
  }

  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }

  handleCheckChange = event => {
    this.setState({
      [event.target.name]: event.target.checked,
    })
  }

  handlePass = event => {
    if(this.state.passShown == false || this.state.passShown == undefined){
      this.setState({passShown: true})
    }else{
      this.setState({passShown: false})
    }
  }

  handleSubmit = event => {
    event.preventDefault()
    this.setState({
      processing: true
    })
    
    if(!this.state.username && !this.state.password) {
      this.setState({
        processing: true
      })
      this.error = "Usuario y contraseña incorrectos, por favor intente de nuevo"
    }else if(!this.state.checkbox) {
      this.setState({
        processing: true
      })
      this.error = "Por favor debes acepta lo términos y condiciones"
    }else{
      handleLogin(this.state.username, this.state.password).then((res) => {
        if(res != undefined && res) {
          localStorage.setItem('username', JSON.stringify(this.state.username));
          this.setState({
            processing: true
          })
          navigate("/welcome")
        }else {
          this.setState({
            processing: false
          })
          this.error = "Nombre de usuario o contraseña están errados."
        }
      })
    }
  }
  /*handleSubmit = event => {
    event.preventDefault()

    handleLogin(this.state)
    if (isLoggedIn()) {
        navigate(`/welcome`)
    }else{
      navigate(`/`)
    }

    if(this.state.password === undefined || this.state.username === undefined){
        this.error = `Debes completar todos los campos`
    }else if(this.state.password !== 'pass' || this.state.username !== 'john'){
      this.error = `Usuario no existe`
    }
  }*/

  render() {

    // if(this.props.location.state){
    //   if(this.props.location.state.isModalOpen){
    //     this.handleModalOpen();
    //   }
    // };
    const page = this.props.data.nodePage;
    const banner = page.relationships.field_custom_blocks[0];
    const section1 = page.relationships.field_custom_blocks[1];
    const section2 = page.relationships.field_custom_blocks[2];
    const section3 = page.relationships.field_custom_blocks[3];

    let linkTest
    
    //isLoggedIn().then
    //  linkTest = <Link className='main-btn' to="/welcome">{banner.field_button}</Link>
    //}else{
      linkTest = <button className='main-btn' onClick={this.handleModalOpen}>{banner.field_button}</button>
    //}
    //login();

    return (
      
  <Layout>
    <Seo title="Home" />
    <div className='hero-banner'>
      <div className='hero-banner-container'>
        <div className='hero-banner-content'>
          <h1 className='main-title'>{banner.field_banner}</h1>
          <img className='hide-desktop' src={banner.relationships.field_banner_image.localFile.publicURL} alt='Evolucion digital e-commerce'/>
          <div dangerouslySetInnerHTML={{__html: banner.body.processed}}/>
          { linkTest }
          {/* <Link className='main-btn' to="#" onClick={this.handleModalOpen}>{banner.field_button}</Link> */}
        </div>
        <img className='hide-mobile' src={banner.relationships.field_banner_image.localFile.publicURL} alt='Evolucion digital e-commerce'/>
      </div>
    </div>

    <section className='columns-grid'>
      <div className='main-container'>
        <h2 className='main-title main-title--section'>{section1.field_secction_title}</h2>
        <div className='columns-grid-container'>
        {section1.field_card_title.map( (title , index) => {
          return (
            <div key={ index } className='columns-grid-item'>
              <img src={section1.relationships.field_card_image[index].localFile.publicURL} alt={section1.field_card_image.alt}/>
               <h3 className='main-title main-title--content numbers'>{title}</h3>
               <div dangerouslySetInnerHTML={{__html: section1.field_card_body[index].processed}}/>
            </div> 
          );
        })}
        </div>
      </div>
    </section>

    <section className='two-columns'>
    <div className='main-container'>
        <h2 className='main-title main-title--section'>{section2.field_secction_title}</h2>
        {section2.field_card_title.map( (title , index) => {
          return (
            <div key={ index } className='two-columns-item'>
              <div className='two-columns-img'>
              <img src={section2.relationships.field_card_image[index].localFile.publicURL} alt={section2.field_card_image.alt}/>
              </div>
              <div className='two-columns-content'>
                <h3 className='main-title main-title--content'>{title}</h3>
                <div dangerouslySetInnerHTML={{__html: section2.field_card_body[index].processed}}/>
              </div>
            </div> 
          );
        })}
        <button className='main-btn main-btn--border' onClick={this.handleModalOpen}>{section2.field_secction_button}</button>
      </div>
    </section>

    <section className='slider'>
      <div className='main-container'>
          <h2 className='main-title main-title--section'>{section3.field_secction_title}</h2>
          <Slider {...settings}>
            {section3.field_card_title.map( (title , index) => {
              return (
                <div key={ index } className='slider-item'>
                  <div className='slider-img'>
                    <img src={section3.relationships.field_card_image[index].localFile.publicURL} alt={section3.field_card_image.alt}/>
                  </div>
                  <div className='slider-content'>
                    <h3 className='main-title main-title--content'>{title}</h3>
                    <div dangerouslySetInnerHTML={{__html: section3.field_card_body[index].processed}}/>
                  </div>
                </div> 
              );
            })}
          </Slider>
      </div>
    </section>
    <ReactModal
          isOpen={this.state.isModalOpen}
          onRequestClose={this.handleModalClose}
          className="modal-container modal-container-form"
           overlayClassName="modal-Overlay"
        >
          <h2 className='main-title main-title--section'>Ingresa tus datos</h2>
          <span className='errorMsg'>{ this.error }</span>
          <form method="post"
              onSubmit={event => {
                this.handleSubmit(event)
              }}>
            <input
              name="username"
              type="text"
              placeholder='Usuario'
              onChange={this.handleInputChange}
            />
            <div className="inputGroup">
              <input
                name="password"
                type={this.state.passShown ? 'text' : 'password' }
                placeholder='Contraseña'
                onChange={this.handleInputChange}
              />
              <div className="showPass" onClick={this.handlePass}><img src={this.state.passShown ? EyeOff : Eye } alt='pass' /></div>
            </div>
            <Link to="/forgotPassword" className="forgotPassword">Recuperar contraseña</Link>
            <div className='custom-check'>
              <label htmlFor='tyc'>Aceptar <Link to='#'>términos y condiciones</Link></label>
              <input
                name="checkbox"
                id="tyc"
                type="checkbox"
                onChange={this.handleCheckChange}
                checked={this.state.checkbox || ''}
              />
              <span className='checkmark'></span>
            </div>
            <button className='main-btn' type="submit">comenzar test</button>
          </form>
            <button className='modal-close' onClick={this.handleModalClose}>Close Modal</button>
    </ReactModal>

  </Layout>
    )
  }
}

export const query = graphql`
  query LandingPage {
    nodePage(title: {eq: "Landing Page"}) {
      title
      relationships {
        field_custom_blocks {
          ... on node__banner {
            field_banner
            body {
              processed
            }
            field_button
            relationships {
              field_banner_image {
                localFile {
                  publicURL
                }
              }
            }
            field_banner_image {
              alt
            }
          }
          ... on node__secction {
            field_secction_title
            field_card_title
            field_card_body {
              processed
            }
            relationships {
              field_card_image {
                localFile {
                  publicURL
                }
              }
            }
            field_card_image {
              alt
            }
            field_secction_button
          }
        }
      }
    }
  }

`;

export default IndexPage
