import * as React from "react"
import { useState, useEffect} from 'react'
import { graphql, navigate } from "gatsby"
import { Helmet } from 'react-helmet'
import ReactModal from 'react-modal'

// import { useState, useEffect } from "react";
import { Radar} from "react-chartjs-2";

import Layout from "../components/layout"
import Seo from "../components/seo"
import Slider from "react-slick"
// import { node } from "prop-types";

ReactModal.setAppElement('#___gatsby')

const last_results_url = `${process.env.GATSBY_DRUPAL_ROOT}/user/last_results?_format=json`;
const settings = {
  dots: false,
  arrows: true,
  infinite: true,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false,
        arrows: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
      }
    }
  ]
};

const RadarOptionsModal = {
  scale: {
    ticks: {
      beginAtZero: true,
      min: 0,
      max: 100,
      stepSize: 20,
      showLabelBackdrop: false,
    },
    pointLabels: {
      fontSize: 10,
      fontFamily: 'Playfair Display',
    }
  },
  legend: {
    display: false
  },
  elements: {
    line: {
      borderWidth: 0,
    }
  }
}

async function fetchUrl() {

  const token = localStorage.getItem('access-token') !== null ? JSON.parse(localStorage.getItem('access-token')) : null;
  // console.log(token);
  const response = await fetch(last_results_url, {
      method: "GET",
      headers: {
          "Accept": "*/*",
          "Content-Type": "application/hal+json",
          "Authorization": `${token.token_type} ${token.access_token}`,
      },
  });

  if (response.ok) {
      const json = await response.json();
      if (json.error) {
        throw new Error(json.error.message);
      }
      // console.log(json);
      return json;
  }
}

const ResultsPage = ({data}) => {
  // this.chartRef = React.createRef();
  // this.state = {
  //   isModalOpen: false,
  // }

    const [chartData, setChartData]  = useState({});    
    const [boxData, setBoxData]  = useState([0,0,0,0]);    
    const [modalIsOpen,setIsOpen] = React.useState(0);
    const [result_opt, setResultData] = useState('');
    function openModal(id) {
      console.log(id)
      setIsOpen(id);
    }
  
    function closeModal(){
      setIsOpen(0);
    }

    let mainData = [
      {
        label: 'Test reciente',
        data: [],
        fill: true,
        backgroundColor: 'rgba(54, 162, 235, 0.5)',
        borderColor: 'rgb(54, 162, 235)',
        pointBackgroundColor: 'rgb(54, 162, 235)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgb(54, 162, 235)'
      },
      {
        label: 'test anterior',
        data: [],
        fill: true,
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
        borderColor: 'rgb(255, 99, 132)',
        pointBackgroundColor: 'rgb(255, 99, 132)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgb(255, 99, 132)',
      }
    ]
    
    const Chart = () => {

      
      let testData1 = [];
      let testData2 = [];

      fetchUrl().then(results =>  {
          if(results){
            if(results[0]){
              let tools1 = results[0].field_complementary_tools[0].value;
              let experience1 = results[0].field_digital_user_experience[0].value;
              let operation1 = results[0].field_operation[0].value;
              let strategy1 = results[0].field_strategy[0].value;

              testData1 = [strategy1,operation1,experience1,tools1];
              mainData[0].data = testData1
            }
          
            if(results[1]){
              let tools2 = results[1].field_complementary_tools[0].value;
              let experience2 = results[1].field_digital_user_experience[0].value;
              let operation2 = results[1].field_operation[0].value;
              let strategy2 = results[1].field_strategy[0].value;
              testData2 = [strategy2,operation2,experience2,tools2];
              mainData[1].data = testData2
            }

            setChartData({
              labels: [
                'Estrategia',
                'Operacion',
                'Experiencia de usuario',
                'Herramientas'
              ],
              datasets: mainData
            });
            return testData1
          }else{
            navigate('/');
          }
          
        });
    }
    useEffect(() => {
       Chart();
     }, []);
    
    let RadarOptions = {
      scale: {
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100,
          stepSize: 20,
          showLabelBackdrop: false,
          fontColor: "rgba(255, 255, 255, .7)",
        },
        pointLabels: {
          fontSize: 12,
          fontFamily: 'Playfair Display',
          fontColor: '#fff',
        },
        gridLines: {
          color: 'rgba(255, 255, 255, .5)'
        },
        angleLines: {
          color: 'rgba(255, 255, 255, .5)'
        }
      },
      legend: {
        display:  false
      },
      elements: {
        line: {
          borderWidth: 0,
          borderColor: '#fff'
        }
      },
    };

    if (typeof window !== `undefined`){
      if(window.innerWidth > 1024){
        RadarOptions.scale.pointLabels.fontSize = 18
      }

    }

    
    let optChart = mainData;
    const page = data.nodePage;
    const banner = page.relationships.field_custom_blocks[0];
    const section1 = page.relationships.field_custom_blocks[1];
    const section2 = page.relationships.field_custom_blocks[2]; 
    const section3 = page.relationships.field_custom_blocks[3];
    const result_options = page.relationships.field_custom_blocks[4];
    const Boxes = () => {
      fetchUrl().then(results =>  {
        if(results){
          if(results[0]){
            let tools1 = results[0].field_complementary_tools[0].value;
            let experience1 = results[0].field_digital_user_experience[0].value;
            let operation1 = results[0].field_operation[0].value;
            let strategy1 = results[0].field_strategy[0].value;
            let res_opt = results[0].field_average[0].value;
            let text_res = '';
            if(res_opt < 26){
              text_res = result_options.field_options[1];
            }else if (res_opt => 26 && res_opt < 70){
              text_res = result_options.field_options[2];
            }else{
              text_res = result_options.field_options[3];
            }
            let newArr = [...boxData];
            newArr = [strategy1,operation1,experience1,tools1];
            setBoxData(
              newArr
            );
            setResultData(
              text_res
            );
          }
        }
        
      })
    }
    useEffect(() => {
      Boxes();
    }, []);

    return (
      <>
      <Helmet>
        <body class='results-template' />
      </Helmet>
      <Layout>
        <Seo title="Results page" />
        <div className='hero-banner hero-banner-chart'>
          <div className='hero-banner-container'>
            <div className='hero-banner-content'>
              <h1 className='main-title'>{banner.field_banner} {result_opt}</h1>
              <div dangerouslySetInnerHTML={{__html: banner.body.processed}}/>
            </div>
            <div className='chart-container'>
              <Radar data={chartData} options={RadarOptions}  width={100} height={100} redraw/>
              {/* <div className='chart-info'>
                <h3>Test score analisis</h3>
                {Object.keys(optChart).map((key, i) => (
                    <div className="chart-info-item" key={ i }><span style= {{ background: optChart[key].pointBackgroundColor}}></span>{optChart[key].label}</div>
                ))}
              </div> */}
            </div>
            <span className='down-arrow'></span>
          </div>
        </div>
        <section className='slider slider-results'>
          <div className='main-container'>
          <h2 className='main-title main-title--section'>{section1.field_secction_title}</h2>
            <Slider {...settings}>
              {section1.field_card_title.map( (title , index) => {
                return (
                  <div key={ index } className='slider-item'>
                    <h3 className='main-title main-title--content'>{title}</h3>
                    <div className='slider-score'>{boxData[index]}</div>
                    <button className='main-btn main-btn--blue' onClick={() =>{openModal(index + 1)}}>ver detalle</button>
                  </div> 
                );
              })}
            </Slider>
          </div>
        </section>
        <section className='columns-grid columns-grid-results'>
        <div className='main-container'>
          <h2 className='main-title main-title--section'>{section2.field_secction_title}</h2>
          <div className='columns-grid-container'>
            {section2.field_card_title.map( (title , index) => {
              return (
                <div key={ index } className='columns-grid-item'>
                  <h3 className='main-title main-title--content numbers'>{title}</h3>
                  <div dangerouslySetInnerHTML={{__html: section2.field_card_body[index].processed}}/>
                </div> 
              );
            })}
          </div>
        </div>
      </section>
            
      {section1.field_card_title.map( (title , index) => {
        return (
          <ReactModal key= { index }
              isOpen={modalIsOpen == index + 1}
              onRequestClose={closeModal}
              className="modal-container modal-container-results"
              overlayClassName="modal-Overlay">
              <div className='modal-columns'>
                <div className='modal-item'>
                  <h3 className='main-title main-title--content'>{ title }</h3>
                  <div className='score'>{boxData[index]}</div>
                  <div dangerouslySetInnerHTML={{__html: section1.field_card_body[index].processed}}/>
                  {/* <button className='main-btn' type="submit">Más resultados</button> */}
                </div>
                <div className='modal-item'>
                  <Radar data={chartData} options={RadarOptionsModal} width={100} height={100}/>
                  {/* <button className='main-btn' type="submit">Más resultados</button> */}
                </div>
              </div>
              
              <button className='modal-close' onClick={closeModal}>Close Modal</button>
          </ReactModal>

        )      

      })} 
      </Layout>
      </>
    )
}

export const query = graphql`
  query ResultsPage {
    nodePage(title: {eq: "Results"}) {      
      title
      relationships {
        field_custom_blocks {
          ... on node__banner {
            field_banner
            body {
              processed
            }
            field_button
            relationships {
              field_banner_image {
                localFile {
                  publicURL
                }
              }
            }
            field_banner_image {
              alt
            }
          }
          ... on node__secction {
            field_secction_title
            field_card_title
            field_card_body {
              processed
            }
            relationships {
              field_card_image {
                localFile {
                  publicURL
                }
              }
            }
            field_card_image {
              alt
            }
            field_secction_button
          }
          ... on node__results_options {
            field_options
          }
        }
      }
    }
  }

`;

export default ResultsPage
