import * as React from "react"
import { Link, navigate } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"
import { handleForgotPassword } from "../services/auth"
class ForgotPassword extends React.Component {

  state = {
    email: '',
    error: '',
  }
  handleInputChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    })
  }
  handleSubmit = event => {
    event.preventDefault()
    this.setState({
      processing: true
    })
    if(!this.state.email) {
      this.setState({
        processing: true
      })
      this.error = "El campo no puede ser vacio"
    }else{
      handleForgotPassword(this.state.email).then((res) => {
        // debug;
        let error = "";
        if(res != undefined && res) {
          // console.log(res);
          // localStorage.setItem('username', JSON.stringify(this.state.username));
          // this.setState({
          //   processing: true
          // })
          this.setState({
            processing: true
          })
          error = "Se ha enviado un mail a el correo suministrado"
        }else {
          this.setState({
            processing: false
          })
          error = "El nombre de usuario no existe"
        }
        this.setState({error});
      })
    }
  }
  
  render() {
    return(
      <Layout>
        <Seo title="Forgot Password" />
        <div className='hero-banner hero-banner-welcome forgotPassword'>
          <div className='hero-banner-container hero-banner-container--oneColumn'>
            <div className='hero-banner-content'>
              <p className='big-text'>¿Has olvidado tu contraseña?</p>
              {/* <span className='errorMsg'>{ this.error }</span> */}
              <span className='errorMsg'>{ !this.state.email ? this.error : this.state.error }</span>
              <form method="post"
                  onSubmit={event => {
                    this.handleSubmit(event)
                  }}>
                <input
                  name="email"
                  type="email"
                  placeholder='E-mail'
                  onChange={this.handleInputChange}
                />
                <button className='main-btn' type="submit">Restaurar contraseña</button>
                <p>o <Link to='/'>Iniciar sesión</Link></p>
              </form>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default ForgotPassword
