import * as React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
// import diagnosticador from '../images/diag-logo.svg'
// import logo from '../images/postobon-logo.svg'

const Header = ({ siteTitle, logoP, altP, logoD, altD }) => (
  <header>
    <div className="main-header">
      <h1 className='second-logo'>
        { siteTitle }
        <Link to="/" >
            <img src={logoD} alt={ altD } />
        </Link>
      </h1>
        <div className='main-logo'>
          <img src={logoP} alt={ altP } />
        </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
  logoP: PropTypes.string.isRequired,
  logoD: PropTypes.string.isRequired,
  altP: PropTypes.string.isRequired,
  altD: PropTypes.string.isRequired
}

Header.defaultProps = {
  siteTitle: ``,
  logoP: ``,
  logoD: ``,
  altP: ``,
  altD: ``,
}

// export const query = graphql`
//   query HeaderMenu {
//     nodeMenu(id: {eq: "25e28666-635e-5fb3-af09-9a278c6fef07"}) {
//       id
//       relationships {
//         field_menu_image {
//           localFile {
//             publicURL
//           }
//         }
//       }
//       field_menu_image {
//         alt
//       }
//     }
//   }
// `;

export default Header
