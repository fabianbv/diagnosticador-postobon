/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

import Header from "./header"
import Footer from "./footer"
import "../styles/styles.scss"

const Layout = ({ children }) => {
  return (
    
    <>
    <StaticQuery
      query={graphql`
      query headerQuery {
        site {
          siteMetadata {
            title
          }
        }
        nodeMenu(id: {}) {
          id
          relationships {
            field_menu_image {
              localFile {
                publicURL
              }
            }
          }
          field_menu_image {
            alt
          }
        }
      }
      `}
      render={data => (
        <>
        <Header siteTitle={data.site.siteMetadata?.title || `Title`}
              logoP={ data.nodeMenu.relationships.field_menu_image[0].localFile.publicURL }
              altP={ data.nodeMenu.field_menu_image[0].alt }
              logoD={ data.nodeMenu.relationships.field_menu_image[1].localFile.publicURL }
              altD={ data.nodeMenu.field_menu_image[1].alt } />

        <main>{children}</main>
        <Footer siteTitle={data.site.siteMetadata?.title || `Title`} />
      </>
      )}
    />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
