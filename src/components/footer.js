import * as React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

import academia from '../images/academia-logo.svg'
import postobon from '../images/postobon-logo-footer.svg'
import central from '../images/central-logo.svg'
import nutrium from '../images/nutrium-logo.svg'



const Footer = ({ siteTitle }) => (
    <footer>
        <div className='main-footer'>
            {/* <img className='main-logo' src={logoWhite} alt={siteTitle} /> */}
            <ul className='footerNav'>
                <li className='footerNav-item'>
                    <Link to='/'>Política de privacidad</Link>
                </li>
                <li className='footerNav-item'>
                    <Link to='/'>Términos y condiciones</Link>
                </li>
                <li className='footerNav-item'>
                    <Link to='/'>Copyright© {new Date().getFullYear()}</Link>
                </li>
            </ul>
            <div className='footerLogos'>
                <img src={academia} alt='Academia Postobon' />
                <div className='footerLogos-grid'>
                    <div className='footerLogos-item'>
                        <img src={academia} alt='Academia Postobon' />
                    </div>
                    <div className='footerLogos-item'>
                        <img src={central} alt='Academia Postobon' />
                    </div>
                    <div className='footerLogos-item'>
                        <img src={postobon} alt='Academia Postobon' />
                    </div>
                    <div className='footerLogos-item'>
                        <img src={nutrium} alt='Academia Postobon' />
                    </div>
                </div>
                <Link to='/'>Copyright© {new Date().getFullYear()}</Link>
            </div>
            {/* <div className='socialNetworks'>
                <a title='facebook' href='https://facebook.com' target='_blank' rel="noreferrer">
                    <img src={facebook} alt='Facebook' />
                </a>
                <a title='whatsapp' href='https://facebook.com' target='_blank' rel="noreferrer">
                    <img src={whatsapp} alt='Whatsapp' />
                </a>
                <a title='twitter' href='https://facebook.com' target='_blank' rel="noreferrer">
                    <img src={twitter} alt='Twitter' />
                </a>
                <a title='youtube' href='https://facebook.com' target='_blank' rel="noreferrer">
                    <img src={youtube} alt='Youtube' />
                </a>
                <a title='instagram' href='https://facebook.com' target='_blank' rel="noreferrer">
                    <img src={instagram} alt='Instagram' />
                </a>
            </div> */}
        </div>
    </footer>
)

Footer.propTypes = {
    siteTitle: PropTypes.string,
}
  
Footer.defaultProps = {
    siteTitle: ``,
}

// export const query = graphql`
//   query FooterMenu {
//     nodeMenu(id: {eq: "e186351f-89f2-5b7a-a1be-7b1d17d3115e"}) {
//       id
//       relationships {
//         field_menu_image {
//           localFile {
//             publicURL
//           }
//         }
//       }
//       field_menu_image {
//         alt
//       }
//     }
//   }
// `;

export default Footer