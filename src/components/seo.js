/**
 * SEO component that queries for data with
 *  Gatsby's useStaticQuery React hook
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react"
import PropTypes from "prop-types"
import { Helmet } from "react-helmet"
import { StaticQuery, graphql } from "gatsby"

function Seo({ description, lang, meta, title }) {

  return (
    <>
    <StaticQuery
      query={graphql`
      query {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
      `}
      render={data => (
        <>
        <Helmet
          htmlAttributes={{
            lang,
          }}
          title={title}
          titleTemplate={data.siteMetadata?.title ? `%s | ${data.siteMetadata?.title}` : null}
          meta={[
            {
              name: `description`,
              content: description || data.siteMetadata,
            },
            {
              property: `og:title`,
              content: title,
            },
            {
              property: `og:description`,
              content: description || data.siteMetadata,
            },
            {
              property: `og:type`,
              content: `website`,
            },
            {
              name: `twitter:card`,
              content: `summary`,
            },
            {
              name: `twitter:creator`,
              content: data.siteMetadata?.author || ``,
            },
            {
              name: `twitter:title`,
              content: title,
            },
            {
              name: `twitter:description`,
              content: description || data.siteMetadata,
            },
          ].concat(meta)}
        />
      </>
      )}
    />
    </>
  )
}

Seo.defaultProps = {
  lang: `en`,
  meta: [],
  description: ``,
}

Seo.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.arrayOf(PropTypes.object),
  title: PropTypes.string.isRequired,
}

export default Seo
