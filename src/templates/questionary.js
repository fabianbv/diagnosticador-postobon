import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { graphql, navigate } from 'gatsby';
import { Helmet } from 'react-helmet'

import Layout from '../components/layout'

import titleImg from '../images/illustrations/deco3.svg'


class Question extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
    }

    state = {
        activeId: null,
        url: null
    }
    
    handleClick = (e, id, url) => {
        this.url = url
        this.setState({ activeId: id })
    };

    handleBack = event => {
        event.preventDefault()
        navigate(-1);
      }
    
    
    render() {
        const quest = this.props.data.nodeQuestion;
        // const menuClass = classNames({
        //     active: this.state.isActive
        //   });
        const nextUrl = this.url || ''
        return (
            <>
            <Helmet>
                <body class='question-template' />
            </Helmet>
            <Layout>

                <div className='question'>
                    <div className='question-container'>
                        <div className='question-progress'><span></span></div>
                        <div className= 'question-index'>
                            <span>{quest.title}</span>
                            <img src={titleImg} alt='question' />
                        </div>
                        <h1 className='question-title'>{quest.field_question}</h1>
                        <p className='question-intro'>Por favor seleccione todas las opciones que apliquen</p>
                        <div className='options-container'>
                            {quest.field_option.map( (preg , index) => {
                                let ref1 = 'results/';
                                let title = 'FIN'
                                console.log(quest)
                                if(quest.relationships.field_option_referenced_question.length !== 0){
                                    if(quest.relationships.field_option_referenced_question[index]){
                                        ref1 = quest.relationships.field_option_referenced_question[index].id;
                                        title = quest.relationships.field_option_referenced_question[index].title;
                                    }                                    
                                }else{
                                    if(quest.field_isfinal === false){
                                        ref1 = quest.relationships.field_next_question.id;
                                        title = quest.relationships.field_next_question.title;
                                    }
                                }
                                return (
                                    <div key={ index } className='options-container-item'>
                                        <div key={ index } className={this.state.activeId === index ? `options-item active` : 'options-item'} aria-hidden="true" onClick={(e)=>{this.handleClick(this, index, ref1)}} data-url={ref1}>{preg}</div>
                                        <a href={ref1} >Pregunta {title} : {preg}</a>
                                    </div>
                                );
                            })}
                        </div>
                        <div className='question-btns'>
                            <a className='main-btn main-btn--green' href={nextUrl ? '/' + nextUrl : '#'}>Siguiente</a>
                            <a className='main-btn main-btn--green' href='#' onClick={this.handleBack}>Volver</a>
                        </div>
                    </div>
                </div>    
            </Layout>
            </>
        );
    }
}


Question.propTypes = {
    data: PropTypes.object.isRequired,
};

export const query = graphql`
    query($QuestionId: String!){
        nodeQuestion(id: { eq: $QuestionId }) {
            id
            title
            field_question
            field_option_values
            field_option
            relationships {
                field_next_question {
                    id
                    title
                }
                field_option_referenced_question {
                    id
                    title
                }
            }
            field_isfinal
        }
    }
`;

export default Question;